
#!perl 
##### 11.00.pl Changed the way the minimum is found it no longer goes from an
##### approach point of view but from a retraction. 
##### Speed up on the switching between fixed and unfiex. 
##### Reset parameter put in place for dealing with overran jobs on HPCs. 

##### 10.06.pl Changed to allow for both areas to be tabulated in the final table### 
##### 10.05.pl Changed the requirement for size'#### 
#use MatServer; # Please enable if running on Linux system 
use strict;
use Getopt::Long qw(GetOptions);
use MaterialsScript qw(:all); 	# Can be used in Material Studio	
use List::Util qw(reduce); 		# Allows the use of lists 
use warnings 'all';
use Math::Trig;
use Data::Dumper; 
use POSIX; # Used for rounding up. 
use Time::HiRes qw( time );

my $start = time(); 
my $linux = 0;
my $reset = 0;
my (@input,@size,@thick,@rot,@dT,$sep);
my (@Length,@min,@area,@atoms,@lim,@longest,@mol);
my (@range,@set,@cent_set,@xytime,@ztime);
my ($zStack,$FZStack); 

### Development Only## 
my @trackedAtoms; #Used for keeping track of 2 atoms from the layers 
our ($bug ,$del,$ani,$timer)=(0,0,0,1); 
my $spacer = "################";

#### Options for linux systems 
GetOptions(
			'linux=i' => \$linux,);
if ($linux == 1 ) {
	use MatServer; 
	GetOptions(
			'size=i{1}' 	 => \$size[0],
			'thick=f{2}'     => \@thick,
			'sep=i{1}' 	 => \$sep, 
			'rot=i{3}'     	 => \@rot,
			'inp=s{2}' 	 =>\@input,
			'deb=i' 	 =>\$bug,
			'del=i'	 	 => \$del,
			'ds=i{2}'	  	 => \@dT,
      		'reset=i{1}'    => \$reset,
			'ani=i'	       	 => \$ani);
			
}else{
# Commands required to pass through the script from command line  
GetOptions(
			'Area_L2=i' 	  => \$size[0], 
			'Thickness_L1=f'  => \$thick[0],
			'Thickness_L2=f'  => \$thick[1], 
			'Separation=i' 	  => \$sep, 
			'Rotation_Step=i' => \$rot[2], 
			'Rotation_Start=i'=> \$rot[0],
			'Rotation_End=i'  => \$rot[1],
			'Layer_1=s' 	  =>\$input[0],
			'Layer_2=s' 	  =>\$input[1],
			'Debug=i' 	  =>\$bug,
			'Delete=i'	  => \$del,
			'Animation=i'	  => \$ani,
			'D_Spacing_L1=i'	  => \$dT[0],
			'D_Spacing_L2=i'	  => \$dT[1]);
}



print "\n\n";	
print "$spacer	$spacer	$spacer\n"; 		
print "#			Calculation Set up \n";
print "$spacer	$spacer	$spacer\n"; 
print "		Size_L2 = $size[0]\n";

print "	   Thickness_L1 = $thick[0]\n";
print "	   Thickness_L2 = $thick[1]\n";
print "		Input 1 = $input[0]\n";
print "		Input 2 = $input[1]\n";
print "	 Rotation_start = $rot[0]\n";
print "	   Rotation_end = $rot[1]\n";
print "	  Rotation_step = $rot[2]\n";
print "	     Seperation = $sep\n";
print "$spacer	$spacer	$spacer\n"; 
print "\n\n";	

####Error Handling Methods 
my $fail=0; ## If the input file/GUI interface doesn't have all the values it will exit the code. 
for my $var (1..2){
	defined ($input[$var-1]) || print "Error ",$fail=1,"\n","No input set for L_$var \n";
	defined ($size[0]) 		 || print "Error ",$fail=2,"\n","No size set for L_$var \n";
	defined ($thick[$var-1]) || print "Error ",$fail=3,"\n","No thickness set for L_$var \n"; 
	defined ($rot[$var-1])	 || print "Error ",$fail=3,"\n","Missing Rotational setting- Check Start,End,Step \n"; 
	defined ($rot[$var])	 || print "Error ",$fail=3,"\n","Missing Rotational setting- Check Start,End,Step \n"; 
	defined ($sep)			 || print "Error ",$fail=4,"\n","Missing Seperation \n"; 
}
if ($fail != 0){ die "Error Found: Please check input files or typos\n";}
#### 
 
my $fh1 = $Documents{"$input[0]".".xsd"}; # Pulls input files from active folder
my $fh2 = $Documents{"$input[1]".".xsd"};
my $Compo = Documents->New("Comp_"."$input[0]"."$input[1]".".xsd"); # Creates new file that will hold
																	# the two layers in one structure 
my @L = Documents->New("Layer1".".xsd");
push (@L,Documents->New("Layer2".".xsd"));
$L[0]->CopyFrom($fh1); # Creates copies of the structural files to make sure it doesn't alter the originals 
$L[1]->CopyFrom($fh2); 
$fh1->Close;
$fh2->Close; 

my $no_rot = ($rot[1] - $rot[0])/$rot[2]; # Calculates number of steps required for rotation  

#Calculating Separation between slabs
#Using half the d-spacing to figure out the separation between the two centroids required 

my $cen_sep = $sep + ((($thick[0]/$dT[0])/2)+(($thick[1]/$dT[1])/2)); # Calculates a separation between L1 and L2 
print "$spacer	$spacer	$spacer\n"; 
print "#			Building Slabs \n";
print "$spacer	$spacer	$spacer\n"; 


# This section attempts to reduce any high aspect ratio shapes from taking place.
# WHERE @min[0][1][2][3] = L1minU , L1minV,L2minU, L2minV 
# WHERE @Length[0][1][2][3] = LengthU_A, LengthV_A,LengthU_B,LengthV_B
# Where @area[0][1][2][3]  = Unit cell Area of L1, Area of Slab L1, Unit Cell Area of L2, , Area of Slab L2 
# Area of the unit cell is taken into account.  

my $req_len = sqrt($size[0]);
my %tracked = ( "Atom_2" => ,
				"Atom_2" => );
for (my $l = 0; $l <2;$l++){
	if ($bug > 2) {
		my $trackedatom = $L[$l]->UnitCell->Atoms(0); # Selects first atom from the layer 
		$tracked{"Atom_$l"} = [sprintf("%.6f",$trackedatom->X),
							   sprintf("%.6f",$trackedatom->Y),
							   sprintf("%.6f",$trackedatom->Z)];
		printf "\nTracked Atom Set $l = x: %f, y: %f,z:%f\n",$tracked{"Atom_$l"}[0],
														  $tracked{"Atom_$l"}[1],
														  $tracked{"Atom_$l"}[2];
	}
 	#Sets the atom coordinates to the hash. 
	$L[$l] = orientation($l);				# Checks The orientation of the cell 
	push(@mol,atoms($L[$l])); 				# Passes layers and find the number of molecules in unit cell. 
	push(@Length, getLattice($l,1)); 			# Extracts Lengths of Unit cells
	push(@area,$L[$l]->Lattice2D->CellArea);		# Passes the Area of the Cell 
	#printf "\nLattice dimensions L%f %f %f\n",($l+1),$Length[$l*2],$Length[$l*2+1]; 
	if ($bug>0){print "Calculating the area\n";} 
	push(@min,(ceil($req_len/$Length[$l*2+1])),		# Divides the required length for a square by the length of the
		  (ceil($req_len/$Length[$l*2+2]))); 		# length lattice for the two layers. 
	#if ($bug>0){printf "The min value is %i and %i",$min[$l*2+1],$min[$l*2+2];} 												
#	push (@min, getDims($Length[$l*2],$Length[$l*2+1]));
	$min[0] = 3;
	$min[1] = 3;					 	# Starts subroutine to reduce the aspect ratio of the unit cell. 	
#	$_ *= $size[$l] for $min[$l*2],$min[$l*2+1]; 		# Multiplies the min value to crate a square 
														# by the desired size of the surface  

	$L[$l]->BuildSuperCell($min[$l*2],$min[$l*2+1]);	# Builds SuperCell 
	push(@atoms,$L[$l]->UnitCell->Atoms->Count); 	# Counts how many atoms have been fixed in the Unitcell. 
	push(@area,$L[$l]->Lattice2D->CellArea); 			# Adds the area of the Slab to the array 
}


if ($bug >= 1){
	print "Number of molecules in the Unit cell of L1 = $mol[0]\n";
	print "Number of molecules in the Unit cell of L2 = $mol[1]\n";
	print "Area of L1 & L2 unit cells = $area[0] , $area[2]\n";
	print "Area of L1 & L2 Slabs = $area[1] , $area[3] \n"; 
}																		# minVA and minVB
print "L1 must be mutiplied by $min[0] x $min[1] along UxV \n"; 
print "L2 must be mutiplied by $min[2] x $min[3] along UxV \n\n";
print "$spacer	$spacer	$spacer\n"; 
print "#		Building Complete \n";
print "$spacer	$spacer	$spacer\n"; 

print "\n";
print "$spacer	$spacer	$spacer\n";  
print "#	Calculating Centroids and Middle Cell \n";
print "$spacer	$spacer	$spacer\n"; 
print "\n\n";

my $sup_lat = $L[0]->SymmetryDefinition; 
#This section looks at finding the lower and upper boundary of the middle cell 
#In the array @range the indexes are as follows 
#range[0,1,2,3]= U_lower,V_lower,U_upper,V_upper
my @L2_range; 
push(@lim,int(3/2),int(3/2),int(($min[2]/2)-1),int(($min[3]/2))-1); # Minus 1 to ensure that the middle cell is selected over the middle 
push(@range,$Length[0],$Length[1]);

push(@L2_range,$Length[3],$Length[4]);

@range = map {$lim[$_] * $range[$_]} 0..$#range;
@L2_range = map {$lim[$_] * $L2_range[$_]} 0..$#L2_range; # Calculates the lower boundary of the L2 units cell 

push(@range,($range[0]+$Length[0]),($range[1]+($Length[1]*sin($Length[2])))); 

@range = map (sprintf("%.6f",$_),@range);   # Ensures all floats are round to 6 sig figures.  
#@range = map {$range[$_]+0.8}0..$#range; # Fixes issues with atoms lying on the unit cell 

#%lines_prop hold the line properties for the two edges of the parallelogram. 
my %lines_prop;

my $u = tan($Length[2]);

$lines_prop{'mean'} = tan($Length[2]);
$lines_prop{'C1'} =  - ($lines_prop{'mean'}*$range[0]) ;
$lines_prop{'C2'} =  - ($lines_prop{'mean'}*$range[2]) ;
if ($bug > 3 ){
	printf "The Mean : %f\n C1 : %f \n C2: %f\n", $lines_prop{'mean'},$lines_prop{'C1'},$lines_prop{'C2'}; 
	print "Unit multipliers 			 : @lim\n"; 
	print "Length and Angles		 : @Length\n";
	print "Ranges (U_L,V_L,U_U,V_U)\n: @range\n"; 
	print "Range for L2 	 			 : @L2_range\n";
}

#Copies Layers into Composite Layers and sets up the sets for the layers. 
# $set[0,1,2,3,4,5] = Set_1,Set_2,Set_3,Set_4,Set_5,Set_6
# Which translates to : L1, L2, FlagsOfL1, ColumnOfL1, AlignmentLayerOfL1,AlignmentLayerOfL2 
for (my $i = 0; $i < 2; $i++) {
	push(@set,$Compo->CopyFrom($L[$i]->UnitCell->Atoms));
	my $ii = $i+1; 
	my $str = "Set_$ii";
	$Compo->CreateSet($str,$set[$i]);
	#push(@cent_set,$Compo->CreateCentroid($Compo->Sets("$str")->Atoms));
	#$cent_set[$i]->IsWeighted = "No";
	#$cent_set[$i]->IsVisible = "No";
}

my $cAtoms = $set[0]->Atoms; # Capture all atoms from current Layer.
my @atom_count;
my ($xyz_table,$xyzsheet); 
if ($bug >= 1){
#	Keeps track of all saved atoms 
	$xyzsheet=Documents->New("XYZ_coordinates_of_atoms.std");
	$xyz_table=$xyzsheet->ActiveSheet;
	$xyz_table->ColumnHeading(0)="All_x";
	$xyz_table->ColumnHeading(1)="All_y";
	$xyz_table->ColumnHeading(2)="x_1";
	$xyz_table->ColumnHeading(3)="y_1";
	$xyz_table->ColumnHeading(4)="x_2";
	$xyz_table->ColumnHeading(5)="y_2";
}

## Prepares surface for column set up 
my (@flags,@column,@display,@alignCent); #Creates arrays for storing atom positions that will be fixed and unfixed. 
foreach my $atom (@$cAtoms){
	my $y = sprintf ("%.6f",$atom->Y); 
	my $x = sprintf ("%.6f",$atom->X);
	my $z = sprintf ("%.6f",$atom->Z); 
	$atom_count[2] += 1 ; 
	
	if($bug >2){
		#printf "Looking for X: %f Y: %f Z: %f\n", $tracked{"Atom_0"}[0],$tracked{"Atom_0"}[1],$tracked{"Atom_0"}[2];
		#printf "Atom Coor   X: %f Y: %f Z: %f\n", $x , $y , $z;	
		if ($x == $tracked{"Atom_0"}[0] and $y == $tracked{"Atom_0"}[1] and $z == $tracked{"Atom_0"}[2]){
			push(@trackedAtoms,$atom); 
			printf "First Tracked Atom found x: %f, y: %f,z:%f\n",$x,$y,$z;
		}
	}
	if ($bug == 1 ){
		$xyz_table->Cell($atom_count[2],0) = "$x"; 
		$xyz_table->Cell($atom_count[2],1) = "$y"; 
	}
	# Creates AlligmentCentroids for the top layer 
	if ($z >= (-($thick[0]/$dT[0]))){
		push(@alignCent,$atom);
	}

	if ($z >= (-($thick[0]/($dT[0])))){
		push(@flags,$atom); 
	}
	
	# Checks the for atoms within the boundary of the unit cell equivalent 
	if ($y >= $range[1] and $y <= $range[3]){
		$atom_count[0] += 1;
		if (checkPosition($x,$y) == 1){ 
			push(@column,$atom);
			if($bug >= 1){
				$xyz_table->Cell($atom_count[0],4) = "$x";
				$xyz_table->Cell($atom_count[0],5) = "$y";  
			}  
		}else{
			#if ($z >= (-($thick[0]/($dT[0]*2)))){
			#	push(@flags,$atom); 
			#}
		}
		
	}else{
		## Creates Flags for sensing any additional interactions which will only pick up half the unit cell thickness worth of atoms 
		#if ($z >= (-($thick[0]/($dT[0]*2)))){
		#	push(@flags,$atom); 
		#}
		$atom_count[1] += 1; 
		if ($del == 1) {
			$atom->Delete;
			#next;
					#Deletes XYZ atoms in all cells that are not in the middle cell.
		}else{
			$atom->Fix('XYZ');
			if ($bug >= 1){
				$xyz_table->Cell($atom_count[1],2) = "$x";
				$xyz_table->Cell($atom_count[1],3) = "$y"; 
			}
			#next; 
			#if ($bug ==1){printf "Number of Atoms FIXED in L1 = $atom_count[1] \n\n";}
			#print "		$x 		$y	\n";
							#Fixes XYZ atoms in all cells that are not in the middle cell.
		}
	}
	if ($bug ==1){
		printf "Number of Atoms NOT FIXED in L1 = $atom_count[0] \n\n";
		printf "Number of Atoms FIXED in L1 = $atom_count[1] \n\n"
	}
}
$Compo->CreateSet("Set_3",\@flags);
$Compo->CreateSet("Set_4",\@column);
$Compo->CreateSet("Set_5",\@alignCent);

if($bug >= 2){
	$xyzsheet->Export("Xyz_coordinates.csv");
	$xyzsheet->Discard;
}
printf "Number of Atoms in L1 = %i \n\n",($set[0]->Atoms->Count);
printf "Number of Atoms in L2 = %i \n\n",($set[1]->Atoms->Count);

## Finds the top D-spacing for the LS 
my $cAtoms_2 = $set[1]->Atoms;
my @alignCent_2;  
foreach my $atom_2 (@$cAtoms_2){
	$atom_count[3] += 1;
	my $y = sprintf ("%.6f",$atom_2->Y); 
	my $x = sprintf ("%.6f",$atom_2->X);
	my $z = sprintf ("%.6f",$atom_2->Z);
	if ($bug>2){
		if ($x == $tracked{"Atom_1"}[0] and $y == $tracked{"Atom_1"}[1] and $z == $tracked{"Atom_1"}[2]){
			push(@trackedAtoms,$atom_2);
			printf "Second Tracked Atom found x: %f, y: %f,z:%f\n",$x,$y,$z;
		}
	}
	if ($z > (-($thick[1]/$dT[1]))){
		push(@alignCent_2,$atom_2);}
	} 
$Compo->CreateSet("Set_6",\@alignCent_2);
$set[1]->Fix('XYZ'); 

if ($bug > 2){
	#Creates tracker set for the first atoms in both layers. 
	$Compo->CreateSet("Atom_i",$trackedAtoms[0]);
	$Compo->CreateSet("Atom_j",$trackedAtoms[1]);
}
push(@cent_set,$Compo->CreateCentroid($Compo->Sets("Set_5")->Atoms)); ## Layer 1 bottom sets 
$cent_set[0]->IsWeighted = "No";
$cent_set[0]->IsVisible = "No";
push(@cent_set,$Compo->CreateCentroid($Compo->Sets("Set_6")->Atoms)); ## Layer 2 Bottom sets 
$cent_set[1]->IsWeighted = "No";
$cent_set[1]->IsVisible = "No";


## VERSION 11.00 Update Set_1 & Set_2 have had centroids removed 
# Thus:
# @xyz array holds the XYZ coordinates for the centroids to the equivalent sets: 
# @xyz[0,1,2,3,4,5]=x5,y5,z5,x6,y6,z6
#Extracts the XYZ coordinates from the Sets to gain the centroids.
# @xyz array holds the XYZ coordinates for the centroids to the equivalent sets: 
# @xyz[0,1,2,3,4,5,6,7,8,9,10,11]=x1,y1,z1,x2,y2,z2,x5,y5,z5,x6,y6,z6

# diffXYZ function takes the XYZ and finds the difference in the XYZ directions
# Cen_dif array is as follows @Cen_dif[0,1,2] = x5-x6, y5-y6 , z5-z6  
my (@xyz,@cen_dif);

#Flips top layer 180 on x axis around the centroid. 
my $Top_Layer = $Compo->DisplayRange->Sets("Set_1")->Atoms;
$Top_Layer->RotateAboutPoint(180, Point(X => 1.0, Y => 0.0, Z => 0.0),$cent_set[0]->CentroidXYZ);
$L[0]->Discard;
$L[1]->Discard; 

push(@xyz,cenXYZ(@cent_set)); 
push(@cen_dif,diffXYZ(@xyz)); 

$Top_Layer->Translate(Point(Z=>(200)));
$Compo->CalculateBonds;
$Top_Layer->Translate(Point(Z=>(-200)));

if ($bug>3){
	print "Var Values are as follows:\n 
	Length[0]:$Length[0]\n
	Length[1]:$Length[1]\n
	Length[2]:$Length[2]\n
	Length[3]:$Length[3]\n
	Length[4]:$Length[4]\n
	Length[5]:$Length[5]\n
	lim[0]:$lim[0]\n
	lim[1]:$lim[1]\n
	lim[2]:$lim[2]\n
	lim[3]:$lim[3]\n";
	
	printf "Movement of Layer 1 has been made by:\n
	X: %f \n
	Y: %f \n
	Z: %f \n", (((cos($Length[5])*$Length[4])+$Length[3])*$lim[2]), (sin($Length[5])*$Length[4])*$lim[3],($cen_sep-abs($cen_dif[2]));
}
$Top_Layer->Translate(Point(Z=>(1+$cen_sep-abs($cen_dif[2])), Y=> abs($cen_dif[1]), X=> abs($cen_dif[0])));

if ($bug >= 2){
	my @xyz_new = cenXYZ(@cent_set); 
	print "Positions of centroids before moving : @xyz\n";
	print "Positions of centroids after moving : @xyz_new\n";
} 


print "\n$spacer	$spacer	$spacer\n";
print "# 	Centroids Aligned and Unit cell Selection Complete\n"; 
print "$spacer	$spacer	$spacer\n"; 
print "\n\n";


###################################### Engine Set-up ########################################################
our $forcite=Modules->Forcite;
$forcite->ChangeSettings(Settings(CurrentForcefield=>"Dreiding",
	ChargeAssignment => "Use current",
	NonPeriodicvdWAtomCubicSplineCutOff => 80, 
	WriteLevel => "Silent"));

print "$spacer	$spacer	$spacer\n";
print "#		 Grid And Tables Set-up \n";
print "$spacer	$spacer	$spacer\n"; 


###################################### Step Set-up ##########################################################
my $grid_spacing=1;   # Grid spacing in Angstroms
my (@energy,@es_energy,@hb_energy,@vdw_energy,@displaceArray,@collisionEnergy,@MINIMA);	 # Array to hold energy values at different Z positions  
my @frame=();

my $doc = $Compo;
my $newStudyTable=Documents->New("InteractionEnergy.std");
my $calcSheet=$newStudyTable->ActiveSheet;

$calcSheet->ColumnHeading(0)="Cell";
$calcSheet->ColumnHeading(1)="Layer1";
$calcSheet->ColumnHeading(2)="Isolated Energy of Flags";
$calcSheet->ColumnHeading(3)="Layer2";
$calcSheet->ColumnHeading(4)="Energy of Layer1";
$calcSheet->ColumnHeading(5)="Interaction Energy";
$calcSheet->ColumnHeading(6)="ES Layer1";
$calcSheet->ColumnHeading(7)="ES Layer2";
$calcSheet->ColumnHeading(8)="HB Layer1";
$calcSheet->ColumnHeading(9)="HB Layer2";
$calcSheet->ColumnHeading(10)="VDW Layer1";
$calcSheet->ColumnHeading(11)="VDW Layer2";
$calcSheet->ColumnHeading(12)="Total ES Energy";
$calcSheet->ColumnHeading(13)="Total HB Energy";
$calcSheet->ColumnHeading(14)="Total VDW Energy";
$calcSheet->ColumnHeading(15)="X axis Displacement";
$calcSheet->ColumnHeading(16)="Y axis Displacement";
$calcSheet->ColumnHeading(17)="Area of Layer 1 ";
$calcSheet->ColumnHeading(18)="Area of Layer 2 ";
$calcSheet->ColumnHeading(19)="Displacement of Layer 1 ";
$calcSheet->ColumnHeading(20)="Rotation";
$calcSheet->ColumnHeading(21)="Total No. Of Atoms Layer 1";
$calcSheet->ColumnHeading(22)="Total No. Of Atoms Layer 2";
$calcSheet->ColumnHeading(23)="Collision Energy";

my ($Debug,$DebugTable); 
if ($bug >= 1 ){
	$DebugTable = Documents->New("DebugTable.std"); 
	$Debug = $DebugTable->ActiveSheet; 
	$Debug->ColumnHeading(0)="Position";
	$Debug->ColumnHeading(1)="X Axis Displacement";
	$Debug->ColumnHeading(2)="Y Axis Displacement";
	$Debug->ColumnHeading(3)="Z Axis Displacement";
	$Debug->ColumnHeading(4)="Degrees of Rotation around Z Axis";
	$Debug->ColumnHeading(5)="Interaction Energy";
	$Debug->ColumnHeading(6)="Total ES Energy";
	$Debug->ColumnHeading(7)="Total HB Energy";
	$Debug->ColumnHeading(8)="Total VDW Energy";
	$Debug->ColumnHeading(9)="Step Size"; 
	$Debug->ColumnHeading(10)="Collision Energy"; 
	
}

############################################# Set up Grid  #########################################
$calcSheet->Cell(0,17)=$area[1];
$calcSheet->Cell(0,18)=$area[3]; #Inputs the area of the Layer
$calcSheet->Cell(0,21)=$atoms[0];
$calcSheet->Cell(0,22)=$atoms[1];

my (@num_steps,@step_size,@grid_length);
for (my $i = 0; $i < 2; $i++) {
	$grid_length[0] = $Length[0];
	$grid_length[1] = $Length[1] * sin($Length[2]);
	$grid_length[2] = $Length[3];
	$grid_length[3] = $Length[4] * sin($Length[5])	; 
	
	#This section finds the longest side of the unit cells 
	if ($grid_length[$i]<$grid_length[$i+2]) {
		push(@longest,$grid_length[$i+2]);
	}else{
		push(@longest,$grid_length[$i]);
	}
	push(@num_steps, int($longest[$i]/$grid_spacing));
	push(@step_size, $longest[$i]/$num_steps[$i]);
}

print "The number of steps in U direction is $num_steps[0]\n";
print "The step size in U direction is $step_size[0]\n";

print "The number of steps in V direction is $num_steps[1]\n";
print "The step size in V direction is $step_size[1]\n";

###################################### In
my $layer1_Doc=Documents->New("layer1.xsd");
#Set up for the Trajectory File. 
if ($ani >= 1){
	$FZStack = Documents->New("FinalZStack.xtd"); 
	$FZStack->Trajectory->AppendFramesFrom($doc);
}

#Set energies of Layer 2 as all zero due to the atoms all being fixed. 
# Copy Layer2 in new document, calculate energy and save in the table	
$layer1_Doc->CopyFrom($doc);
$layer1_Doc->UnitCell->Sets("Set_2")->Atoms->Delete;
$calcSheet->Cell(0,3)=$layer1_Doc;

# Calculate energy of Layer1 and save in table
######################################################
#This section calculates the energy of the bottom attoms of layer 1 that are not in the unit cell. 
#print "About to Calculate the Isolated energies \n";
my $l1_flag = getEnergy($layer1_Doc,$layer1_Doc->DisplayRange->Sets("Set_4")->Atoms,
						$layer1_Doc->DisplayRange->Sets("Set_3")->Atoms,$forcite,"collision",0);
	
$calcSheet->Cell(0,2) = $l1_flag;
$layer1_Doc = getEnergy($layer1_Doc,$layer1_Doc->DisplayRange->Sets("Set_4")->Atoms,
						$layer1_Doc->DisplayRange->Sets("Set_3")->Atoms,$forcite,"unit",0);

$calcSheet->Cell(0,4)=($layer1_Doc->PotentialEnergy);
$calcSheet->Cell(0,7)=($layer1_Doc->ElectrostaticEnergy);
$calcSheet->Cell(0,9)=($layer1_Doc->HydrogenBondEnergy);
$calcSheet->Cell(0,11)=($layer1_Doc->VanDerWaalsEnergy);


###################################### SystematicCalculations ###########
my $Setupduration = time() - $start;
print "Time to setup structure: $Setupduration s \n"; 
print "$spacer	$spacer	$spacer\n";
print "#		Grid And Tables Set-up Complete\n";
print "$spacer	$spacer	$spacer\n";
print "\n\n";
print "\n\n";

print "$spacer	$spacer	$spacer\n";
print "#		Starting Calculations \n";
print "$spacer	$spacer	$spacer\n\n"; 
#($num_steps[0],$num_steps[1])=(2,2);
$Compo->Centroids->Delete;
$Compo->Sets("Set_2")->Delete;
$Compo->Sets("Set_6")->Delete;
$Compo->Sets("Set_5")->Delete;
my ($counter,$frame,$newtime,$counter2,$tick,$progress) = (0,0,0,0,0,0); 
if ($bug >=2) {
	my $layerSet=Documents->New("layerSetUP.xsd");
	$layerSet ->CopyFrom($doc);
}

#$num_steps[0]= 1;
#$num_steps[1]= 1;
my $switchTrigger;
my $tablePosition = 0;
my $total_num_steps = ($num_steps[0] * $num_steps[1] * (($rot[1]-$rot[0])/$rot[2]));
for (my $r = $rot[0]; $r < $rot[1]; $r += $rot[2]){
	undef @MINIMA;
	my ($alldoc,$tempLayer,$flag,$unit)=copyStructure($input[0],$input[1],$r);
	$tempLayer->RotateAbout($r,Point(Z => 1.0, Y => 0.0, X => 0.0));
	$tempLayer->Translate(Point(X=>-($longest[0]/2),Y=>-($longest[1]/2)));

	for (my $x = 0; $x < ($num_steps[0]); $x++) {
		for (my $y = 0; $y < ($num_steps[1]); $y++) {
			$tempLayer->Translate(Point(X=>$step_size[0]*$x,Y=>$step_size[1]*$y));
			my $copytime = time() - $newtime; 
			if ($ani>1){
			eval{
				our $zStack = Documents->New("Temp_ZStack.xtd");
				$zStack->Trajectory->AppendFramesFrom($alldoc);
				1;}or do{
				print "Tried to create animation but could not\n"; 
			};
			}
			#my $mytemp = Documents->New("Temp_Structure_"."$x _ $y".".xsd"); 
			#$mytemp->CopyFrom($alldoc);  
			$calcSheet->Cell($counter,0)="Position"."_$x"."_$y";
			$calcSheet->Cell($counter,15)="$x";
			$calcSheet->Cell($counter,16)="$y";
			$calcSheet->Cell($counter,20)="$r"; 
			print "#Current Position: $x , $y\n "; 
			my $progressPerCent = ((++$progress)/ $total_num_steps)* 100;  
			print "#Progressed: $progressPerCent % \n";	
			
			my $dZ = 0.1; 
			my $constant = 0.05; 
			my ($interaction_Energy,$vdw_energy,$es_energy,$hb_energy);
			my ($collisionEnergy_temp,$dCollisionEnergy); 
			my ($overshot, $displacement, $move) = (0,0,0,0,0,0);
			my $z_limit = 100; 
			undef @energy;			# Empty the array 
			undef @es_energy;
			undef @hb_energy;
			undef @vdw_energy;
			undef @displaceArray;
			undef @collisionEnergy;
			$xytime[0] = time(); 
			for (my $k = 0; $k < 100; ++$k) {
				
				print"$spacer\n";
				$frame += 1; 
				print "\n#					Counter_$frame			\n";
				$ztime[0] = time();
				if ($k == 0){
					$dZ = 0.5
				}
				if ($x == 0 and $y==0 and $k == 0){
					$switchTrigger = 0;
				}else{
					$switchTrigger = 1; 
				}
				$tempLayer->Translate(Point(Z=>$dZ));
				$displacement += $dZ;
				$ztime[1] = time();
				my $collisionAtoms = getEnergy($alldoc,$unit,$flag,$forcite,"collision",$switchTrigger);

				my $unitAtoms;
				$ztime[2]=time();
				$collisionEnergy_temp = ($collisionAtoms - ($calcSheet->Cell(0,2)));
			
				#printf "Calculated the Collision Energy : %f\n",$collisionEnergy_temp;

				push(@collisionEnergy,$collisionEnergy_temp);
				push(@displaceArray,$displacement); 
				# If bug 10 is enabled the separation will also be calculated for the Unit cell
				#during the steepest descent method. 
				$ztime[10] = time(); 
				
				if ($bug >= 9){
					printf "Debug with Extra interaction Energy is on, Bug = %i",$bug;
					my $unitAtoms = getEnergy($alldoc,$unit,$flag,$forcite,"unit");
					saveToDebug($alldoc,
								$Debug,$x,$y,$r,$displacement,
								$dZ,$collisionEnergy_temp,$counter2,$bug);
				}elsif($bug < 5 and $bug >=1){
					saveToDebug(0,$Debug,$x,$y,$r,$displacement,
					$dZ,$collisionEnergy_temp,$counter2,$bug);
				}
				$ztime[11] = time(); 
				#Calculates the energy difference between different points based on the overshot
				#$overshot:index -- 0:k-1; 1:k-2; 2:k-4; 
				$dCollisionEnergy = $collisionEnergy_temp - $collisionEnergy[$k-2**$overshot];
				$ztime[3]=time();
				if ($bug >= 1) {
					if ($bug >= 10){
						printf "#Interaction Energy = %f \n",($alldoc->PotentialEnergy - ($calcSheet->Cell(0,4)));
					}
					print "##Position"."_$x"."_$y \n";
					print "#Collision Energy  					= $collisionEnergy_temp \n";
					printf "#Difference in Energy between %f and %f = $dCollisionEnergy\n" , $displacement, $displaceArray[$k-2**$overshot];
					print "#Step Size 							= $dZ \n";
					print "#Displacement is 					= $displacement \n";
				}
				$frame[$k]=$frame;
				++$counter2;	
				$ztime[5] = time(); 
				if ($timer == 1){
					printf "Time taken to record data to debug file and calculate unit cell %f \n", $ztime[11]-$ztime[10];
					printf "Time taken to push Data to Array = %f\n",$ztime[10]-$ztime[2];
					printf "Time taken to move slab 		= %f\n", ($ztime[1]-$ztime[0]);
					printf "Time Taken to carry out calculation = %f\n",($ztime[2]-$ztime[1]); 
					printf "Time Taken to prepare steps sizes = %f\n",($ztime[4]-$ztime[3]); 
				}
				print "OS : $overshot \n";
				print "$spacer\n";

				if ($dZ < 0.001 and $dCollisionEnergy < 0.01 and $dCollisionEnergy > -0.01){
					last;
				}


				#If the graident is Positive overshot is triggered.
				if ($dCollisionEnergy >= 0.000001 and $dZ != 0.5){
					$overshot +=1;
				}else{
					$overshot = 0;
				}
				if ($overshot > 3){
					last;
				}
				if($overshot == 0 and $dCollisionEnergy < 1000 and $k > 2 and $collisionEnergy_temp < 1000){
					$dZ = calculateDZ($dZ,$dCollisionEnergy,$constant);
				}elsif($dCollisionEnergy < 1000 and $k>2 and $collisionEnergy_temp < 1000){
					($dZ,$constant,$displacement) = moveLayerBack($overshot,$tempLayer,$displacement,$dZ,
																\@displaceArray,$k,$constant);
				}
				$ztime[4]=time();
				if ($dCollisionEnergy > 1000){
					$dZ = 0.5; 
				}
				#Conditions for reaching convergence 
				#if ($overshot > 2){
				#	$dZ = 0.001; 
				#}
			}
			#print "About to run the last position calculation\n";
			#print "Energy Array : @collisionEnergy\n";
			#print "Diplacement Array : @displaceArray\n";
			my $n = reduce { $collisionEnergy[$a] < $collisionEnergy[$b] ? $a : $b } 0..$#collisionEnergy; # Finds Lowest Interaction Energy Step
			#printf "Index Value calculated : %i\n", $n;
			#$tempLayer->Translate(Point(Z=>($displaceArray[$n] - $displacement)));
			#my $unitAtoms = getEnergy($alldoc,$unit,$flag,$forcite,"unit",1);
			#($interaction_Energy,$es_energy,$hb_energy,$vdw_energy) = componentEnergies($unitAtoms,$calcSheet);

			#printf "Interaction Calculations 		= %f\n",$interaction_Energy;
			#printf "HB _ Energy 					= %f\n",$hb_energy;
			#printf "ES_Energy 						= %f\n",$es_energy;
			#printf "VdW_Energy						= %f\n",$vdw_energy;
			my @Array = ($x,$y,$displaceArray[$n]);
			#printf "Array of the Positions = @Array \n";
			push (@MINIMA, \@Array);
			print "\n $spacer $spacer $spacer\n";
			#print "#Lowest Energy 				= $interaction_Energy \n";
			print "$spacer $spacer $spacer\n";
			print "\n";
			print "$spacer Approach Scan Complete $spacer \n";
			print "\n";

			#$calcSheet->Cell($counter,5)=$interaction_Energy;	#Based on the lowest value it uses that index value. 
			#$calcSheet->Cell($counter,12)=$es_energy;
			#$calcSheet->Cell($counter,13)=$hb_energy;
			#$calcSheet->Cell($counter,14)=$vdw_energy;
			#$calcSheet->Cell($counter,19)=$displaceArray[$n];
			#$calcSheet->Cell($counter,23)=$collisionEnergy[$n];
			$newtime = time(); 
			++$counter;

			if ($ani >= 1){
				my $to = $FZStack; 
				my $from = $zStack;
				my $trjTo = $to->Trajectory;
				$FZStack ->CurrentFrame = $tick;
				if ($ani >= 2){
					$trjTo->InsertFramesFrom($from);
				}else{
					$trjTo->InsertFramesFrom($from, Frames(Start => ($frame[$n]), End => $frame[$n]));
				}
				$zStack->Discard;
			
			}
			if ($timer==1){
				printf "Time taken to do Z-Stack 			=%f  \n", (time() - $xytime[0]); 
			}
			$tempLayer->Translate(Point(X=>-$step_size[0]*$x, Y=>-$step_size[1]*$y, Z=>-$displacement));
			
			++$tick; 
			if ($displacement > 50){
				last;
			}
		}
	}
	print "$spacer	$spacer	$spacer\n";
	print "#	Rotation $r Complete \n";
	print "$spacer	$spacer	$spacer\n";
	
	print "Table postion = $tablePosition \n";
	getEnergyMinima(\@MINIMA, $alldoc,$tempLayer,$flag,$unit,$calcSheet,$forcite,\@step_size,$tablePosition);
	$tablePosition += ($num_steps[0]* $num_steps[1]);
	$alldoc->Discard; 
	if ($bug > 0){
		$DebugTable-> Export("Debug of"."$input[1]".".csv");
	}
	$area[0] = int($area[0]);
	$area[2] = int($area[2]);
	$newStudyTable->Export("$input[0]"."_"."$area[0]"."vs"."_$input[1]"."_$area[2]"."_$reset".".csv"); 
}
print "\n\n";
print "$spacer	$spacer	$spacer\n";
#$layer1_Doc->Discard;
if ($bug > 0){
	$DebugTable->Discard;
}
$newStudyTable->Discard;
my $time1=localtime;
print "#	Calculation ended $time1 \n";
my $duration = time() - $start;
print "Execution time(): $duration s\n";
print "Done!!\n";
print "$spacer	$spacer	$spacer\n";

reset;

my $rand = int(rand(100)); 

sub copyStructure {
#Copy the original structure and define the layer to move   	
	my ($i,$j,$r)=@_;

	my $all_doc=Documents->New("Start_Structure_of"."_$i"."_$j"."_$r.xsd"); # Names all positions with File name. 

	$all_doc->CopyFrom($Compo);# Copy the whole cell (both layers) to another document 
	
	my $tempLayer= $all_doc->DisplayRange->Sets("Set_1")->Atoms;# Define the layer to move
	my $flags= $all_doc->DisplayRange->Sets("Set_3")->Atoms;# Define the layer to move
	my $unit= $all_doc->DisplayRange->Sets("Set_4")->Atoms;# Define the layer to move
	
	return ($all_doc, $tempLayer,$flags,$unit);
}

sub diffXYZ {
	my @xyz = @_;
	my @numbers = (0,1,2);
	my @dif; 
	foreach my $i (@numbers){
		push(@dif,($xyz[$i]-$xyz[$i+3]));
	}	
	return(@dif); 
}

sub cenXYZ {
	my @set =@_;
	my @center; 
	if ($bug ==1){
		print "Set used are..."; 
		print "@set\n";
	}
	foreach my $set_i (@set){
		push(@center,($set_i->CentroidXYZ->X,
			      $set_i->CentroidXYZ->Y,
			      $set_i->CentroidXYZ->Z));
	}
	if ($bug >= 1 ){print "The XYZ for centroid as ARRAYS for @set are @center\n";}
	return(@center);
}

sub getDims {
	my @lat = @_; 
	my $dif = $lat[0] / $lat[1];  
	if ($bug >= 1){
	print "\n Array passed to get_dims2 = @lat\n";
	print "\n $lat[0] $lat[1]\n";
	print "\n Difference is  $dif\n";
	}
	if ($dif < 1){
	return (round(1 / $dif,0), 1);
	}else{
	return (1, round($dif,0));
	} 
}

sub getLattice {
	#### Function used to gather lattice parameters of surface and return UV.
	#### Function will resolve for V vector using the angle of the cell. 
	my $i = $_[0]; 
	my $triger = $_[1];
	my $lat = $L[$i]->SymmetryDefinition;
	my $angle = deg2rad($L[$i]->Lattice2D->AngleTheta); 
	my @len = $lat->LengthU;
	push (@len,(sin($angle)*$lat->LengthV));
	if ($triger == 1){					# If the orientation is being reset it will return the original
		return($len[0],$lat->LengthV,$angle);          # Length of U as opposed to the resolved. 
	}else{
	return($len[0],$len[1]);
	}
}

sub orientation {
	my $l = $_[0];
	
	my $orientation = $L[$l]->SymmetryDefinition->OrientationConvention;
	if ($orientation eq "U along X, V in XY plane") {
    	print "Lattice uses U along X , V in XY plane\n";
	}else{
	my ($L_U,$L_V,$angle) = getLattice($l,1); 
	$L[$l]->UnbuildSurface;
	my $buildSurface = Tools->SurfaceBuilder->Surface;
	$buildSurface->SetPlaneGroup("p 1");
	$buildSurface->SetMeshParameters($L_U, $L_V, $angle);
	$buildSurface->Build($L[$l], Settings(
	OrientationConvention => "U along X, V in XY plane"));
	}
	return($L[$l]);
}
sub atoms {
	my $layer = $_[0]; 
	my $atom = $layer->UnitCell->Atoms(0);
	my $fragment = $atom->Fragment->Atoms; 
	my $pattern = Documents->New("Pattern.xsd");
	$pattern->CopyFrom($fragment); 
	$pattern->CalculateBonds; 
	my $mol = Tools->Patterns->FindPatterns->Find($layer,$pattern); 
	$pattern->Discard; 

return ($mol->Count);

}

sub atomDistances {
# Measures the distance between two atoms. UNDER DEVELOPMENT
	my $atom_1 = $_[0];
	my $atom_2 = $_[1];
}

sub getCoordinates {
#Finds XYZ Coordinates for atom provided. 
	my $atom = $_[0];
	my $y = sprintf ("%.6f",$atom->Y); 
	my $x = sprintf ("%.6f",$atom->X);
	my $z = sprintf ("%.6f",$atom->Z); 
	return ($x , $y, $z); 
}

sub round {
    my ($x) = @_;
    my $a = 10**0;

    return (int($x / $a + (($x < 0) ? -0.5 : 0.5)) * $a);
}

sub checkPosition {
	my $x = $_[0];
	my $y = $_[1];
	my $mx = $lines_prop{'mean'} * $x;
	my @equation; 
	$equation[0] = $mx + $lines_prop{'C1'} - $y; 
	$equation[1] = $mx + $lines_prop{'C2'} - $y; 
	if ($bug>2){ 
	#printf "\nEquation1 value : %f \n", $equation[0]; 
	#printf "\nEquation1 value : %f \n", $equation[1];  
	}
	if ($lines_prop{'mean'} < 0){
		if ($equation[1]>= 0 and $equation[0] <= 0){
			return (1);
		}else{
			return (0); 
		}
	}
	if ($equation[0]>= 0 and $equation[1] <= 0){
		return (1);
	}else{
		return (0); 
	}
}

sub getEnergy {
	my ($alldoc,$unit,$flags,$forcites,$case,$k) = @_; 
	#printf "FUNCTION getEnergy\n";
	#printf "In this situation the case is %s\n",$case;
	my @time;
	if ($case eq "collision"){
			$time[0] = time(); 
			if ($bug <= 9){
				if ($k == 0){
					#print "\nCollision Mode - Flags unfixed\n";
					$unit->Fix('XYZ');
					$flags->Unfix('XYZ');
				}
			}else{
				#print "\n Collision Mode - flags unfixed \n";
				$unit->Fix('XYZ');
				$flags->Unfix('XYZ');
			}
			$time[1] = time(); 
			$forcite->Energy->Run($alldoc);
			$time[2] = time();
			printf "Time taken to run Forcite = %f\n ", $time[2] - $time[1];
			#printf "Time taken to fix and Unfix atoms = %f \n", $time[1] - $time[0];
			#printf "Energy of The collision = %f",$alldoc->PotentialEnergy;
			return ($alldoc->PotentialEnergy);
	}elsif ($case eq "unit"){
			if ($k == 0 ){
				print " Atoms have been switched \n"; 
				$flags->Fix('XYZ');
				$unit->Unfix('XYZ');
			}
			$time[3] = time();
			$forcite->Energy->Run($alldoc); 
			$time[4] = time();
			printf "Time taken to run Forcite - $case mode = %f\n ", $time[4] - $time[3];
			#printf "Energy of the Unit = %f\n",$alldoc->PotentialEnergy;
			return($alldoc);
	}
}


sub calculateDZ {
    my $dZ = $_[0];
    my $dCollisionEnergy = $_[1];
    my $constant = $_[2];
	
	#print "FUNCTION calculateDZ\n";
	#printf "Values passed = %f,%f,%f\n",$dZ,$dCollisionEnergy,$constant;
    eval{
        $dZ = abs(log(abs($dCollisionEnergy/$dZ)*100))*$constant; 
		return ($dZ);
        1;
    }or do{
        print "Check Collision Energy, dZ forced to 0.001\n"; 
        return (0.001); 
    }
}

sub moveLayerBack {
	# Finds the distance it needs to move based on the overshot value passed to it. 
    my ($case,$tempLayer,$displacement,$dZ,$displaceArray,$k,$constant) = @_;
	#print "FUNCTION  moveLayerBack\n";
	#print "\nInputs for moveLayerBack @_\n"; 
    my $move = $displacement - @{$displaceArray}[$k-($case*2-1)];  # Case = 1 = k-1 , 2 = k - 3; 
    $displacement += -$move;
	$tempLayer->Translate(Point(Z=>-$move));
    $dZ= $dZ*0.25;	
	$constant = $constant/2; 
	#printf "Function return: dZ = %f, constant = %f, displacement = %f, move = %f",$dZ, $constant,$displacement,$move;
	return ($dZ,$constant,$displacement); 
}

sub lowestValueIndex {
	my @array = @{$_[0]};
	my $index = reduce { $array[$a] < $array[$b] ? $a : $b } 0..$#array; 
	return ($index);
}

sub componentEnergies {
	my ($alldoc,$calcSheet) = @_; 
	#print "Started componentEnergies\n";
	my $interaction_Energy=($alldoc->PotentialEnergy - ($calcSheet->Cell(0,4)));
	my $es_energy=($alldoc->ElectrostaticEnergy - ($calcSheet->Cell(0,7)));
	my $hb_energy=($alldoc->HydrogenBondEnergy - ($calcSheet->Cell(0,9)));
	my $vdw_energy=($alldoc->VanDerWaalsEnergy - ($calcSheet->Cell(0,11)));
	printf "\nEnergy : %f \n",$interaction_Energy; 
	return($interaction_Energy,$es_energy,$hb_energy,$vdw_energy); 
}


sub saveToDebug { 
	my ($alldoc,$Debug,$x,$y,$r,$displacement,$dZ,$collisionEnergy_temp,$counter2,$bug) = @_;
	my ($interaction_Energy,$es_energy,$hb_energy,$vdw_energy) = ("NaN","NaN","NaN","NaN");
	#print "FUNCTION : SavetoDebug\n";
	#print "bug $bug\n";
	if ($bug == 10){
		($interaction_Energy,$es_energy,$hb_energy,$vdw_energy) = componentEnergies($alldoc,$calcSheet);
		#printf "Component Energies = %f , %f ,%f , %f\n",$interaction_Energy,$es_energy,$hb_energy,$vdw_energy;
	}
	$Debug->Cell($counter2,0) = "Position"."_$x"."_$y";
	$Debug->Cell($counter2,1) = "$x"; 
	$Debug->Cell($counter2,2) = "$y"; 
	$Debug->Cell($counter2,3) = "$displacement"; 
	$Debug->Cell($counter2,4) = "$r";
	$Debug->Cell($counter2,5) = "$interaction_Energy"; 
	$Debug->Cell($counter2,6) = "$es_energy"; 
	$Debug->Cell($counter2,7) = "$hb_energy";
	$Debug->Cell($counter2,8) = "$vdw_energy";
	$Debug->Cell($counter2,9) = "$dZ";
	$Debug->Cell($counter2,10) = "$collisionEnergy_temp";
}

sub getEnergyMinima {
	my ($MINIMA, $alldoc,$tempLayer,$flag,$unit,$calcSheet,$forcite,$step_size,$tablePosition) = @_;

	my @init;
	my @movement;
	my $counter = 0;
	print "Counter = $counter\n";
	foreach my $x (@MINIMA){
		if (!@init){
			@movement = ($x->[0], $x->[1], $x->[2]);
		}else{
			@movement = map { $x->[$_] - $init[$_]} 0..$#init; 
		}
		@init = ($x->[0], $x->[1], $x->[2]);
		print "Current Position = @init \n";
		print "Move by = @movement\n"; 
		print "Step Size = @step_size\n";
		$tempLayer->Translate(Point(
									X=>$step_size[0]*$movement[0],
									Y=>$step_size[1]*$movement[1], 
									Z=>$movement[2]));
		
		my $unitAtoms = getEnergy($alldoc,$unit,$flag,$forcite,"unit",$counter);
		saveToMainTable($calcSheet,$tablePosition,componentEnergies($unitAtoms,$calcSheet),$x->[2]);
		++$counter;
		++$tablePosition;
	}
	$tempLayer->Translate(Point(
									X=>-$step_size[0]*$init[0],
									Y=>-$step_size[1]*$init[1], 
									Z=>-$init[2]));
}


sub saveToMainTable {
	my ($calcSheet,$counter,$interaction_Energy,$es_energy,$hb_energy,$vdw_energy,$displacement) = @_;
	$calcSheet->Cell($counter,5)=$interaction_Energy;	#Based on the lowest value it uses that index value. 
	$calcSheet->Cell($counter,12)=$es_energy;
	$calcSheet->Cell($counter,13)=$hb_energy;
	$calcSheet->Cell($counter,14)=$vdw_energy;
	$calcSheet->Cell($counter,19)=$displacement;
	#$calcSheet->Cell($counter,23)=$collisionEnergy[$n];
	}

my @qoute = ("Two things are infinite: the universe and human stupidity; and I'm not sure about the universe. - Albert Einstein",
			"There is no law except the law that there is no law. - John Archibald Wheeler",
			"Falsity in intellectual action is intellectual immorality. - Thomas Chrowder Chamberlin",
			"The saddest aspect of life right now is that gathers knowledge faster than society gathers wisdom. ― Isaac Asimov",
			"Science without religion is lame, religion without science is blind. ― Albert Einstein",
			"A man who dares to waste one hour of time has not discovered the value of life. ― Charles Darwin",
			"The good thing about science is that it's true whether or not you believe in it. ― Neil deGrasse Tyson",
			"Nothing in life is to be feared, it is only to be understood. Now is the time() to understand more, so that we may fear less. ― Marie Curie",
			"You cannot teach a man anything; you can only help him discover it in himself. - Galileo",
			"Imagination is more important than knowledge. - Albert Einstein",
			"Shall I refuse my dinner because I do not fully understand the process of digestion? - Oliver Heaviside",
			"If I have seen further it is by standing on the shoulders of Giants. ― Isaac Newton",
			"One, remember to look up at the stars and not down at your feet. Two, never give up work. Work gives you meaning and purpose and life is empty without it. Three, if you are lucky enough to find love, remember it is there and don't throw it away. ― Stephen Hawking",
			"The science of today is the technology of tomorrow - Edward Teller",
			"Scientists have become the bearers of the torch of discovery in our quest for knowledge - Stephen Hawking",
			"Our scientific power has outrun our spiritual power. We have guided missiles and misguided men -Martin Luther King, Jr.",
			"Medicine is a science of uncertainty and an art of probability - William Osler",
			"Research is what I'm doing when I don't know what I'm doing -Wernher von Braun ",
			"Science is about knowing; engineering is about doing - Henry Petroski",
			"No amount of experimentation can ever prove me right; a single experiment can prove me wrong - Albert Einstein");


print "\n",$qoute[int(rand(20))]; 
