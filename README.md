# Surface Surface Interaction Model

Surface-Surface Interaction Model. 

Used to calculate the interaction energy between two surfaces. 

This script runs through Biovia's Materials Studio Software Suite. 


# Installation 

The script can be used through the Materials Studio (MS) Graphical User Interface (GUI) or via the comman line/terminal (Linux supported).

The script can be added to a library and a User Menu has been generated for easy use in the GUI.

With the script imported into the folder select the Scripting -> Library menu.

![Menu](./images/menu.png)

Add File and select from the dropdown. 

![Add File](./images/Add_script.png)

Then move to the User Menu tab and select Archive Import and select the User_Menu.xml file in this repo. Once that is loaded you will see it under User. Select it and press the ... under command. This connects the User Menu to the script. 

![User Menu](./images/user_menu.png) 
![Select Script](./images/Select_script.png)

The default arguments can be changed via the "Arguments" Tab./

![Set Arguments](./images/set_arguments.png)


# Example 

The following example is for illustration purpose, none of the values here have been validated or used for any studies. 

## Surface Generation:

Surface are cleaved from crystal. The thickness is noted and the orientation is set to U along X, V along XY Plane.
![Cleave](./images/Cleave.png)
![Orient](./images/set_orient.png)

With the new surface generated selected go to User -> SSIM_V11.0D

![Start](./images/start_script.png)

This will open the SSIM Menu with the default arguments: 

![SSIM MENU](./images/SSIM_menu.png)

Set values to the desired set of inputs. Please note that the separation is set to 0. This triggers the Faster set of calculations that start the surfaces from a point of contact and finds the energy minima by moving apart. setting the sepration to any value above zero will cause the model to bring the two surfaces together in a step wise fashion will be SIGNIFICANTLY slower and it's only there for development and testing. 

![Values Set](./images/Set_values.png)

The output is saved in a new folder and contains several different files. 

Summarised output is saved as a `.csv` and the name is generated as follows `FileNameL1_AreaL1vs_FileNameL2_AreaL2_StatusCode`. 
Status code is used when running on HPCs to allow job resets. Still in development. `Comp_SameFileNameAsAbove` contains the atomic coordinates of the STARTING state of the two layers, with their Sets still selected. 
`SSIM.pl.out` contains the STDOut of the model. 

![Output Files](./images/output.png)

<img src="./images/starting_structure.png" width=450>

It is advised you use gitlab.com/AlexAMC/ssimtool to analyse the output data. 
